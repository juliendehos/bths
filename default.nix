{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "bths" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv


