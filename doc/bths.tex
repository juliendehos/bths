% pdflatex -shell-escape bths.tex

\documentclass[a4paper,oneside]{article}
\usepackage[frenchb]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amssymb} 
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{minted}
\usepackage{graphicx}


%-------------------------------------------------------------------------

\title{Une implémentation du jeu Breakthrough en Haskell}

\author{Julien Dehos}

%-------------------------------------------------------------------------
       
\begin{document}

\maketitle

\section{Le jeu Breakthrough}

\paragraph{}
Jeu de plateau de type jeu de dames. 

\paragraph{}
Initialement, le plateau est composé de deux rangées de pions noirs en
haut et de deux rangées de pions blancs en bas. Le joueur blanc commence
le jeu.

\paragraph{}
Le but est de placer un de ses pions de l'autre côté du plateau.

\paragraph{}
Chaque joueur joue à tour de rôle. Un pion peut être déplacé sur la
case avant, si celle-ci est libre. Un pion peut également être déplacé
sur l'une des deux cases diagonales avant; si la case contenait un
pion adverse, celui-ci est capturé.

\begin{center}
\includegraphics[width=12cm]{breakthrough.png}
\end{center}

\paragraph{}
Dans l'implémentation proposée ici, les pions blancs sont joués par
l'utilisateur et les pions noirs par une IA basique. Les déplacements
sont réalisés à la souris en cliquant sur les cases voulues. Les cases
possibles/sélectionnées sont affichées en surbrillance selon un code
couleur. Une boite de dialogue affiche le vainqueur une fois la partie
terminée.

\paragraph{}
Le code Haskell complet fait environ 280 lignes. Le code C++
équivalent fait presque 1500 lignes.

\newpage




%-------------------------------------------------------------------------

\section{Gestion du plateau de jeu}

Le module \texttt{BthsPlateau} permet de gérer un plateau de jeu en
respectant les règles : créer un plateau, jouer des coups et d'obtenir
des informations sur un plateau.

\subsection{Déclaration du module}
\begin{minted}[frame=single]{haskell}
module BthsPlateau
( Plateau, Couleur(..), Case, Coup
, newPlateau, jouerCoupM, getVainqueur, getCoupsPossibles
, getNoirsBlancs, getCouleurCourante, getCourantsAdverses
) where
import Data.Maybe
\end{minted}


\subsection{Déclaration des types de données}
\begin{minted}[frame=single]{haskell}
-- couleur des pions : noir ou blanc
data Couleur = Noir | Blanc deriving (Eq, Show)

-- coordonnees d'une case sur le plateau
type Case = (Int, Int)

-- coup a jouer : case initiale, case finale
type Coup = (Case, Case)

-- plateau de jeu : couleur courante, cases courantes, cases adverses
type Plateau = (Couleur, [Case], [Case])
\end{minted}


\subsection{Fonctions publiques}
\begin{minted}[frame=single]{haskell}
-- retourne un plateau avec la configuration de depart 
newPlateau :: Plateau
newPlateau = (Blanc, blancs, noirs)  -- les blancs commencents
    where blancs = [(i,j) | i<-[6..7], j<-[0..7]]  -- 2 rangees de blancs en bas
          noirs  = [(i,j) | i<-[0..1], j<-[0..7]]  -- 2 rangees de noirs en haut
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne la couleur du joueur qui doit jouer
getCouleurCourante :: Plateau -> Couleur
getCouleurCourante (couleur, _, _) = couleur                
\end{minted}

\begin{minted}[frame=single]{haskell}
-- teste si une couleur a gagne la partie (et retourne la couleur le cas echeant)
getVainqueur :: Plateau -> Maybe Couleur
getVainqueur plateau = 
    let (noirs, blancs) = getNoirsBlancs plateau
    in if null noirs   -- teste si les joueurs ont encore des pions
       then Just Blanc
       else if null blancs
            then Just Noir
            else  -- teste si un blanc en haut ou si un noir en bas
                let (iBlanc, _) = minimum blancs
                    (iNoir, _) = maximum noirs
                in if iNoir>=7 
                   then Just Noir
                   else if iBlanc<=0 then Just Blanc else Nothing                
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne les coups pouvant etre joues par le joueur courant
getCoupsPossibles :: Plateau -> [Coup]
getCoupsPossibles plateau@(couleur, courants, adverses) =
    if (getVainqueur plateau)/=Nothing  -- si un joueur a gagne, aucun coup possible
    then []
    else
        -- calcule tous les deplacements meme si hors plateau ou capture invalide
        let inc = if couleur==Blanc then -1 else 1
            f a (i,j) = ((i,j),(i+inc,j)):((i,j),(i+inc,j-1)):((i,j),(i+inc,j+1)):a
            coups = foldl f [] courants
        -- retient les coups valides uniquement
        in [((i0,j0),(i1,j1)) | ((i0,j0),(i1,j1)) <- coups, 
            i1>=0, i1<=7, j1>=0, j1<=7, 
            not (j0==j1 && (i1,j1) `elem` adverses),
            not (i1==(i0+inc) && j1`elem` [j0-1,j0,j0+1] && (i1,j1)`elem` courants)]
                    
\end{minted}

\begin{minted}[frame=single]{haskell}
-- essaie de joueur un coup (si le coup est valide, retourne le nouveau plateau)
jouerCoupM :: Plateau -> Maybe Coup -> Maybe Plateau
jouerCoupM plateau coupM = 
    if coupM==Nothing
    then Nothing
    else let coup = fromJust coupM
         in if coup `elem` (getCoupsPossibles plateau)
         then Just (jouerCoupOk plateau coup)
         else Nothing                
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne la liste des pions noirs et la liste des pions blancs du plateau
getNoirsBlancs :: Plateau -> ([Case], [Case])
getNoirsBlancs (couleur, courants, adverses) = 
    if couleur==Noir then (courants, adverses) else (adverses, courants)                
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne la liste des pions courants et la liste des pions adverses
getCourantsAdverses :: Plateau -> ([Case], [Case])
getCourantsAdverses (_, c, a) = (c,a)                
\end{minted}



\subsection{Fonctions privées}
\begin{minted}[frame=single]{haskell}
-- retourne la couleur suivante (pour passer au coup suivant)
getCouleurSuivante :: Couleur -> Couleur
getCouleurSuivante p = if p==Blanc then Noir else Blanc
\end{minted}

\begin{minted}[frame=single]{haskell}
-- joue un coup suppose valide et retourne le nouveau plateau
jouerCoupOk :: Plateau -> Coup -> Plateau
jouerCoupOk (couleur, courants, adverses) (caseInitiale, caseFinale) =
    (couleur', courants', adverses')
    where -- passe a la couleur suivante
          couleur' = getCouleurSuivante couleur 
          -- si deplacement sur un pion adverse, le supprime
          courants'  = [c | c <- adverses, c/=caseFinale]          
          -- ajoute la nouvelle case et supprime l'ancienne
          adverses' = caseFinale : [c | c <- courants, c/=caseInitiale]
\end{minted}




\newpage



%-------------------------------------------------------------------------

\section{Gestion d'un joueur}

Le module \texttt{BthsJoueur} implémente la stratégie de jeu d'un
joueur. Pour un joueur humain, il suffit d'utiliser les cases que
l'utilisateur a cliquées. Pour une IA, il suffit d'implémenter le
calcul du coup à jouer.

\subsection{Déclaration du module}
\begin{minted}[frame=single]{haskell}
module BthsJoueur
( Joueur(..), newHumain, newIa, nouveauCoup, clicCase, getCoupM, getCaseInitialeM
) where
import BthsPlateau
import Data.Maybe
\end{minted}

\subsection{Déclaration des types de données}

Pour un humain, on utilise la case initiale choisie, la case finale
choisie et la liste des cases initiales possibles.

Pour une IA, on utilise uniquement le coup calculé.

\begin{minted}[frame=single]{haskell}
data Joueur = Humain (Maybe Case) (Maybe Case) [Case] 
            | Ia (Maybe Coup)
\end{minted}


\subsection{Fonctions publiques}
\begin{minted}[frame=single]{haskell}
newHumain :: Joueur
newHumain = Humain Nothing Nothing []
\end{minted}


\begin{minted}[frame=single]{haskell}
newIa :: Joueur
newIa = Ia Nothing
\end{minted}


\begin{minted}[frame=single]{haskell}
-- commence un nouveau coup a jouer
nouveauCoup :: Joueur -> Plateau -> Joueur
nouveauCoup (Ia coup) p =  Ia (getCoupIA p)  -- une IA calcule tout de suite son coup
nouveauCoup (Humain _ _ _) p = Humain Nothing Nothing $ map fst $ getCoupsPossibles p
\end{minted}


\begin{minted}[frame=single]{haskell}
-- prend en compte (ou pas) les clics de l'utilisateur
clicCase :: Joueur -> Case -> Joueur
clicCase (Ia coup) _ = (Ia coup)  -- pour une IA, on ignore le clic
clicCase (Humain c0 _ cp) c = 
  if c `elem` cp  -- si clic sur une case initiale, on la retient
  then (Humain (Just c) Nothing cp) 
  else (Humain c0 (Just c) cp) -- sinon on retient la case comme la case finale
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne le coup eventuellement choisi par le joueur
getCoupM :: Joueur -> Maybe Coup
getCoupM (Ia coup) = coup
getCoupM (Humain c0 c1 _) = 
  if c0/=Nothing && c1/=Nothing then Just ((fromJust c0), (fromJust c1)) else Nothing
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne la case initiale eventuellement choisie par le joueur
getCaseInitialeM :: Joueur -> Maybe Case
getCaseInitialeM (Ia k) = if k==Nothing then Nothing else Just ((fst.fromJust) k)
getCaseInitialeM (Humain c0 _ _) = if c0==Nothing then Nothing else c0
\end{minted}




\subsection{Fonctions privées}

\begin{minted}[frame=single]{haskell}
-- IA basique : si possible, fait une capture, sinon joue le premier coup possible
getCoupIA :: Plateau -> Maybe Coup
getCoupIA p =
    let coups = getCoupsPossibles p
    in if null coups
       then Nothing
       else let adverses = (snd.getCourantsAdverses) p
                captures = [ c | c@(_,c1) <- coups, c1 `elem` adverses]
            in Just (if null captures then head coups else head captures)
\end{minted}


\newpage


%-------------------------------------------------------------------------

\section{Gestion d'une partie de jeu}
Le module \texttt{BthsJeu} implémente la partie de jeu, notamment le
jeu à tour de rôle de chaque joueur sur le plateau.

\subsection{Déclaration du module}
\begin{minted}[frame=single]{haskell}
module BthsJeu
( Jeu, newJeu, jeuGetVainqueurM, jeuGetCoupsPossibles
, jeuGetNoirsBlancs, jeuGetCaseInitialeM, jeuClicCase
) where
import BthsPlateau
import BthsJoueur
import Data.Maybe
\end{minted}

\subsection{Déclaration des types de données}
\begin{minted}[frame=single]{haskell}
-- plateau, joueur courant, joueur adverse
type Jeu = (Plateau, Joueur, Joueur)
\end{minted}


\subsection{Fonctions publiques}
\begin{minted}[frame=single]{haskell}
-- retourne un jeu initial
newJeu :: Jeu
newJeu = let p = newPlateau in (p, nouveauCoup newHumain p, newIa)
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne s'il y a un vainqueur
jeuGetVainqueurM :: Jeu -> Maybe Couleur
jeuGetVainqueurM (p, _, _) = getVainqueur p
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne les coups a jouer possibles
jeuGetCoupsPossibles :: Jeu -> [Coup]
jeuGetCoupsPossibles (p, _, _) = getCoupsPossibles p
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne la liste des pions noirs et la liste des pions blancs
jeuGetNoirsBlancs :: Jeu -> ([Case], [Case])
jeuGetNoirsBlancs (p, _, _) = getNoirsBlancs p
\end{minted}

\begin{minted}[frame=single]{haskell}
-- retourne l'eventuelle case initiale choisie
jeuGetCaseInitialeM :: Jeu -> Maybe Case
jeuGetCaseInitialeM (_, j0, _) = getCaseInitialeM j0
\end{minted}

\begin{minted}[frame=single]{haskell}
-- gere le clic sur une case et fait derouler le jeu 
jeuClicCase :: Jeu -> Case -> Jeu
jeuClicCase (p,j0, j1) caseCliquee = 
    -- transmet le clic au joueur courant puis lui demande son eventuel coup
    let j0' = clicCase j0 caseCliquee
        coupM = getCoupM j0'
        pM' = jouerCoupM p coupM
    in if pM'==Nothing   -- si le joueur n'a pas joue de coup valide,
       then (p, j0', j1) -- alors c'est toujours a lui de jouer
       -- si le joueur a joue un coup valdie, on passe au tour suivant
       else let p' = fromJust pM'  
            in (p', nouveauCoup j1 p', j0')
\end{minted}



\newpage


%-------------------------------------------------------------------------

\section{Programme principal, interface graphique}

Le programme principal gère l'interface graphique et les données du
jeu. Tous les traitements "impurs" (effets de bords) sont regroupés
dans ce fichier.

\paragraph{}
L'interface graphique est implémentée en \textit{gtk}. Elle est
composée simplement d'une fenêtre principale contenant une zone de
dessin (\texttt{drawingArea}). Le programme gère l'événement
d'affichage (\texttt{exposeEvent}) et de clic souris
\texttt{buttonPressEvent}.

\paragraph{}
L'interface graphique en \textit{gtk} fonctionne par événements,
c'est-à-dire avec une boucle principale implicite et des fonctions de
rappels. Par conséquent, les données de jeu doivent être accessibles
et modifiables par ces fonctions de rappels. Pour cela, on utilise une
monade \texttt{IORef} pour gérer une donnée de type \texttt{Jeu} de
façon mutable.

\subsection{Fonction \texttt{main}}

\begin{minted}[frame=single]{haskell}
import BthsJeu
import BthsPlateau
import Graphics.UI.Gtk
import Graphics.Rendering.Cairo
import Data.IORef
import Data.Maybe

main :: IO ()
main = do
  initGUI
  let largeur = 800
      hauteur = 600
  jeuRef <- newIORef newJeu
  -- window
  window <- windowNew
  widgetSetSizeRequest window largeur hauteur
  set window [ windowTitle := "Breakthrough", windowResizable := False ]
  -- drawingArea
  canvas <- drawingAreaNew
  containerAdd window canvas
  widgetShowAll window
  -- expose event
  drawWindow <- widgetGetDrawWindow canvas
  canvas `on` exposeEvent $ tryEvent $ liftIO $
         gererExposeEvent drawWindow jeuRef largeur hauteur
  -- double click event
  canvas `on` buttonPressEvent $ tryEvent $ do
    DoubleClick <- eventClick
    liftIO $ return ()
  -- left button event
  canvas `on` buttonPressEvent $ tryEvent $ do
    LeftButton <- eventButton
    (x,y) <- eventCoordinates
    liftIO $ gererButtonPressEvent drawWindow jeuRef largeur hauteur x y 
  -- destroy event      
  onDestroy window mainQuit
  -- main loop
  mainGUI
\end{minted}


\subsection{Gestion des événements}

La fonction d'affichage dessine les différents éléments du plateau de jeu courant dans la zone de dessin.

\begin{minted}[frame=single]{haskell}
-- gestion de l'affichage
gererExposeEvent :: DrawWindow -> IORef (Jeu) -> Int -> Int -> IO ()
gererExposeEvent window jeuRef largeur hauteur = do
  jeu <- readIORef jeuRef
  -- affiche le plateau de jeu
  let (noirs, blancs) = jeuGetNoirsBlancs jeu
      coups = jeuGetCoupsPossibles jeu
      caseInitialeM = jeuGetCaseInitialeM jeu
  renderWithDrawable window $ dessinerPlateau largeur hauteur
  renderWithDrawable window $ dessinerPions blancs largeur hauteur 1 1 1
  renderWithDrawable window $ dessinerPions noirs largeur hauteur 0 0 0
  renderWithDrawable window $ dessinerCases (map fst coups) largeur hauteur 1 0 0
  if caseInitialeM/=Nothing
  then do  
    -- affiche les cases jouables selon le code couleur prevu
    let caseInitiale = fromJust caseInitialeM
        casesFinales = map snd $ filter (\ (c0, _) -> c0==caseInitiale) coups
    renderWithDrawable window $ dessinerCases [caseInitiale] largeur hauteur 1 1 1
    renderWithDrawable window $ dessinerCases casesFinales largeur hauteur 0 0 1
  else return ()
\end{minted}

Pour gérer le clic, on transmet la case cliquée au \texttt{Jeu} pour
dérouler la partie de jeu. On vérifie ensuite s'il y a un vainqueur et
on met à jour l'affichage.

\begin{minted}[frame=single]{haskell}
-- gestion du clic souris
gererButtonPressEvent :: DrawWindow->IORef(Jeu)->Int->Int->Double->Double->IO()
gererButtonPressEvent window jeuRef largeur hauteur x y = do
    let i = floor $ y*8/(fromIntegral hauteur)
        j = floor $ x*8/(fromIntegral largeur)
    liftIO $ do 
         jeu <- readIORef jeuRef
         -- deroule la partie de jeu
         let jeu' = jeuClicCase jeu (i,j)
             vainqueurM = jeuGetVainqueurM jeu'
         writeIORef jeuRef jeu'
         actualiser window largeur hauteur
         -- teste s'il y a un vainqueur
         if vainqueurM==Nothing 
         then return ()
         else do
           afficherMessage $ "Vainqueur : " ++ (show.fromJust) vainqueurM
           writeIORef jeuRef newJeu
           actualiser window largeur hauteur
\end{minted}




\subsection{Fonctions auxiliaires}

\begin{minted}[frame=single]{haskell}
--  dessine une ligne dans une zone de dessin gtk
dessinerLigne :: Int -> Int -> Int -> Int -> Render()
dessinerLigne x0 y0 x1 y1 = do
  moveTo (fromIntegral x0) (fromIntegral y0)
  lineTo (fromIntegral x1) (fromIntegral y1)
\end{minted}

\begin{minted}[frame=single]{haskell}
-- dessine un plateau vide dans une zone de dessin gtk
dessinerPlateau :: Int -> Int -> Render()
dessinerPlateau largeur hauteur= do
  -- dessine le fond
  setSourceRGB 0.3 0.6 0.3
  rectangle 0 0  (fromIntegral largeur)  (fromIntegral hauteur)
  fill
  stroke
  -- dessine la grille
  setSourceRGB 0 0 0
  mapM_ (\ x -> dessinerLigne x 0 x hauteur) [0, (largeur `div` 8) .. largeur]
  mapM_ (\ y -> dessinerLigne 0 y largeur y) [0, (hauteur `div` 8) .. hauteur]
  stroke
\end{minted}


\begin{minted}[frame=single]{haskell}
-- dessine une liste de pions d'une couleur donnee
dessinerPions :: [Case] -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerPions pions largeur hauteur r g b = 
  mapM_ (\ (i,j) -> dessinerPion i j largeur hauteur r g b) pions
\end{minted}

\begin{minted}[frame=single]{haskell}
-- dessine un pion
dessinerPion :: Int -> Int -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerPion i j largeur hauteur r g b = do
  -- dessine un disque a la position correspondant a la case (i,j)
  setSourceRGB r g b
  save
  translate x y 
  scale s s
  arc 0 0 1 0 7
  fill
  restore
  stroke
    where l = (fromIntegral largeur)
          h = (fromIntegral hauteur)
          dx = l/8
          dy = h/8
          x = ((fromIntegral j)+0.5)*dx
          y = ((fromIntegral i)+0.5)*dy
          s = 0.4 * (min dx dy)
\end{minted}

\begin{minted}[frame=single]{haskell}
-- affiche des cases en surbrillance 
dessinerCases :: [Case] -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerCases cases largeur hauteur r g b = do
  -- dessine un rectangle de la couleur donnee autour des cases
  setSourceRGB r g b
  mapM_ (\(i,j) -> rectangle ((fromIntegral j)*dx) ((fromIntegral i)*dy) dx dy) cases
  stroke
    where l = (fromIntegral largeur)
          h = (fromIntegral hauteur)
          dx = l/8
          dy = h/8
\end{minted}

\begin{minted}[frame=single]{haskell}
-- demande de mettre a jour l'affichage
actualiser :: DrawWindow -> Int -> Int -> IO ()
actualiser win l h = drawWindowInvalidateRect win (Rectangle 0 0 l h) False  
\end{minted}

\begin{minted}[frame=single]{haskell}
-- affiche un message dans une boite de dialogue gtk
afficherMessage :: String -> IO ()
afficherMessage s = do
  dialog <- messageDialogNew Nothing [] MessageInfo ButtonsOk s
  dialogRun dialog
  widgetDestroy dialog
\end{minted}


%-------------------------------------------------------------------------

\end{document}

