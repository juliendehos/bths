# bths

 [![Build status](https://gitlab.com/juliendehos/bths/badges/master/build.svg)](https://gitlab.com/juliendehos/bths/pipelines)

 A Haskell implementation of the [Breakthrough board
game](https://en.wikipedia.org/wiki/Breakthrough_%28board_game%29).

![](breakthrough.png)

## Run

install ghc, gtk2hs

```
runhaskell Bths.hs
```

## Using Nix

```
cabal2nix . > bths.nix
nix-build
./result/bin/bths
```


