-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module BthsJeu
( Jeu, newJeu, jeuGetVainqueurM, jeuGetCoupsPossibles
, jeuGetNoirsBlancs, jeuGetCaseInitialeM, jeuClicCase
) where

import BthsPlateau
import BthsJoueur
import Data.Maybe

type Jeu = (Plateau, Joueur, Joueur)

newJeu :: Jeu
newJeu = let p = newPlateau in (p, nouveauCoup newHumain p, newIa)

jeuGetVainqueurM :: Jeu -> Maybe Couleur
jeuGetVainqueurM (p, _, _) = getVainqueur p

jeuGetCoupsPossibles :: Jeu -> [Coup]
jeuGetCoupsPossibles (p, _, _) = getCoupsPossibles p

jeuGetNoirsBlancs :: Jeu -> ([Case], [Case])
jeuGetNoirsBlancs (p, _, _) = getNoirsBlancs p

jeuGetCaseInitialeM :: Jeu -> Maybe Case
jeuGetCaseInitialeM (_, j0, _) = getCaseInitialeM j0

jeuClicCase :: Jeu -> Case -> Jeu
jeuClicCase (p,j0, j1) caseCliquee = 
    let j0' = clicCase j0 caseCliquee
        coupM = getCoupM j0'
        pM' = jouerCoupM p coupM
    in if isNothing pM'
       then (p, j0', j1)
       else let p' = fromJust pM'
            in (p', nouveauCoup j1 p', j0')



