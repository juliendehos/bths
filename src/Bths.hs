-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

import BthsJeu
import BthsPlateau
import Graphics.UI.Gtk hiding (rectangle)
import Graphics.Rendering.Cairo
import Data.IORef
import Data.Maybe
import Control.Monad

main :: IO ()
main = do
  initGUI
  let largeur = 800
      hauteur = 600
  jeuRef <- newIORef newJeu
  -- window
  window <- windowNew
  widgetSetSizeRequest window largeur hauteur
  set window [ windowTitle := "Breakthrough", windowResizable := False ]
  -- drawingArea
  canvas <- drawingAreaNew
  containerAdd window canvas
  widgetShowAll window
  -- expose event
  drawWindow <- widgetGetDrawWindow canvas
  canvas `on` exposeEvent $ tryEvent $ liftIO $
         gererExposeEvent drawWindow jeuRef largeur hauteur
  -- double click event
  canvas `on` buttonPressEvent $ tryEvent $ do
    DoubleClick <- eventClick
    liftIO $ return ()
  -- left button event
  canvas `on` buttonPressEvent $ tryEvent $ do
    LeftButton <- eventButton
    (x,y) <- eventCoordinates
    liftIO $ gererButtonPressEvent drawWindow jeuRef largeur hauteur x y 
  -- destroy event      
  onDestroy window mainQuit
  mainGUI

gererExposeEvent :: DrawWindow -> IORef Jeu -> Int -> Int -> IO ()
gererExposeEvent drawWindow jeuRef largeur hauteur = do
    jeu <- readIORef jeuRef
    let (noirs, blancs) = jeuGetNoirsBlancs jeu
        coups = jeuGetCoupsPossibles jeu
        caseInitialeM = jeuGetCaseInitialeM jeu
    renderWithDrawable drawWindow $ dessinerPlateau largeur hauteur
    renderWithDrawable drawWindow $ dessinerPions blancs largeur hauteur 1 1 1
    renderWithDrawable drawWindow $ dessinerPions noirs largeur hauteur 0 0 0
    renderWithDrawable drawWindow $ dessinerCases (map fst coups) largeur hauteur 1 0 0
    when (isJust caseInitialeM) $ do
      let caseInitiale = fromJust caseInitialeM
          casesFinales = map snd $ filter (\ (c0, _) -> c0==caseInitiale) coups
      renderWithDrawable drawWindow $ dessinerCases [caseInitiale] largeur hauteur 1 1 1
      renderWithDrawable drawWindow $ dessinerCases casesFinales largeur hauteur 0 0 1

gererButtonPressEvent :: DrawWindow -> IORef Jeu -> Int -> Int -> Double -> Double -> IO ()
gererButtonPressEvent drawWindow jeuRef largeur hauteur x y = do
    let i = floor $ y*8 / fromIntegral hauteur
        j = floor $ x*8 / fromIntegral largeur
    liftIO $ do 
         jeu <- readIORef jeuRef
         let jeu' = jeuClicCase jeu (i,j)
             vainqueurM = jeuGetVainqueurM jeu'
         writeIORef jeuRef jeu'
         actualiser drawWindow largeur hauteur
         unless (isNothing vainqueurM) $ do
            afficherMessage $ "Vainqueur : " ++ (show.fromJust) vainqueurM
            writeIORef jeuRef newJeu
            actualiser drawWindow largeur hauteur

actualiser :: DrawWindow -> Int -> Int -> IO ()
actualiser win l h = drawWindowInvalidateRect win (Rectangle 0 0 l h) False  

afficherMessage :: String -> IO ()
afficherMessage s = do
  dialog <- messageDialogNew Nothing [] MessageInfo ButtonsOk s
  dialogRun dialog
  widgetDestroy dialog
    
dessinerLigne :: Int -> Int -> Int -> Int -> Render()
dessinerLigne x0 y0 x1 y1 = do
  moveTo (fromIntegral x0) (fromIntegral y0)
  lineTo (fromIntegral x1) (fromIntegral y1)

dessinerPlateau :: Int -> Int -> Render()
dessinerPlateau largeur hauteur= do
  -- dessine le fond
  setSourceRGB 0.3 0.6 0.3
  rectangle 0 0  (fromIntegral largeur)  (fromIntegral hauteur)
  fill
  stroke
  -- dessine la grille
  setSourceRGB 0 0 0
  mapM_ (\ x -> dessinerLigne x 0 x hauteur) [0, (largeur `div` 8) .. largeur]
  mapM_ (\ y -> dessinerLigne 0 y largeur y) [0, (hauteur `div` 8) .. hauteur]
  stroke

dessinerPions :: [(Int, Int)] -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerPions pions largeur hauteur r g b = 
  mapM_ (\ (i,j) -> dessinerPion i j largeur hauteur r g b) pions

dessinerPion :: Int -> Int -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerPion i j largeur hauteur r g b = do
  setSourceRGB r g b
  save
  translate x y 
  scale s s
  arc 0 0 1 0 7
  fill
  restore
  stroke
    where (l, h, dx, dy) = infoRectangle largeur hauteur
          x = (fromIntegral j + 0.5)*dx
          y = (fromIntegral i + 0.5)*dy
          s = 0.4 * min dx dy

dessinerCases :: [Case] -> Int -> Int -> Double -> Double -> Double -> Render()
dessinerCases cases largeur hauteur r g b = do
  setSourceRGB r g b
  mapM_ (\ (i,j) -> rectangle (fromIntegral j * dx) (fromIntegral i * dy) dx dy) cases
  stroke
    where (l, h, dx, dy) = infoRectangle largeur hauteur

infoRectangle :: Int -> Int -> (Double,Double,Double,Double)
infoRectangle largeur hauteur = (l, h, l/8, h/8)
    where l = fromIntegral largeur
          h = fromIntegral hauteur

