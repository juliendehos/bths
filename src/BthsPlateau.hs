-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module BthsPlateau
( Plateau, Couleur(..), Case, Coup
, newPlateau, jouerCoupM, getVainqueur, getCoupsPossibles
, getNoirsBlancs, getCouleurCourante, getCourantsAdverses
) where

import Data.Maybe

data Couleur = Noir | Blanc deriving (Eq, Show)
type Case = (Int, Int) 
type Coup = (Case, Case)
type Plateau = (Couleur, [Case], [Case])

newPlateau :: Plateau
newPlateau = (Blanc, blancs, noirs)
    where blancs = [(i,j) | i<-[6..7], j<-[0..7]]
          noirs  = [(i,j) | i<-[0..1], j<-[0..7]]                   

getCouleurCourante :: Plateau -> Couleur
getCouleurCourante (couleur, _, _) = couleur

getCoupsPossibles :: Plateau -> [Coup]
getCoupsPossibles plateau@(couleur, courants, adverses) =
    if isJust (getVainqueur plateau)
    then []
    else
        let inc = if couleur==Blanc then -1 else 1
            f acc (i,j) = ((i,j),(i+inc,j)):((i,j),(i+inc,j-1)):((i,j),(i+inc,j+1)):acc
            coups = foldl f [] courants
        in [((i0,j0),(i1,j1)) | ((i0,j0),(i1,j1)) <- coups, 
            i1>=0, i1<=7, j1>=0, j1<=7, 
            not (j0==j1 && (i1,j1) `elem` adverses),
            not (i1==(i0+inc) && j1 `elem` [j0-1,j0,j0+1] && (i1,j1) `elem` courants)]
    
jouerCoupM :: Plateau -> Maybe Coup -> Maybe Plateau
jouerCoupM plateau coupM = 
    if isNothing coupM
    then Nothing
    else let coup = fromJust coupM
         in if coup `elem` getCoupsPossibles plateau
         then Just (jouerCoupOk plateau coup)
         else Nothing

getVainqueur :: Plateau -> Maybe Couleur
getVainqueur plateau = 
    let (noirs, blancs) = getNoirsBlancs plateau
    in if null noirs 
       then Just Blanc
       else if null blancs
            then Just Noir
            else
                let (iBlanc, _) = minimum blancs
                    (iNoir, _) = maximum noirs
                in if iNoir>=7 
                   then Just Noir
                   else if iBlanc<=0 then Just Blanc else Nothing

getNoirsBlancs :: Plateau -> ([Case], [Case])
getNoirsBlancs (couleur, courants, adverses) = 
    if couleur==Noir then (courants, adverses) else (adverses, courants)

getCourantsAdverses :: Plateau -> ([Case], [Case])
getCourantsAdverses (_, c, a) = (c,a)

-- private
getCouleurSuivante :: Couleur -> Couleur
getCouleurSuivante p = if p==Blanc then Noir else Blanc

jouerCoupOk :: Plateau -> Coup -> Plateau
jouerCoupOk (couleur, courants, adverses) (caseInitiale, caseFinale) =
    (couleur', courants', adverses')
    where couleur' = getCouleurSuivante couleur 
          adverses' = caseFinale : [c | c <- courants, c/=caseInitiale]
          courants'  = [c | c <- adverses, c/=caseFinale]
          
