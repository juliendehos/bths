-- Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
-- This work is free. You can redistribute it and/or modify it under the
-- terms of the Do What The Fuck You Want To Public License, Version 2,
-- as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

module BthsJoueur
( Joueur(..), newHumain, newIa
, nouveauCoup, clicCase, getCoupM, getCaseInitialeM
) where

import BthsPlateau
import Data.Maybe

data Joueur = Humain (Maybe Case) (Maybe Case) [Case] | Ia (Maybe Coup)

newHumain :: Joueur
newHumain = Humain Nothing Nothing []

newIa :: Joueur
newIa = Ia Nothing

nouveauCoup :: Joueur -> Plateau -> Joueur
nouveauCoup (Ia coup) p =  Ia (getCoupIA p)
nouveauCoup (Humain{}) p = 
    Humain Nothing Nothing $ map fst $ getCoupsPossibles p

clicCase :: Joueur -> Case -> Joueur
clicCase (Ia coup) _ = Ia coup
clicCase (Humain c0 _ cp) c = 
    if c `elem` cp then Humain (Just c) Nothing cp else Humain c0 (Just c) cp

getCoupM :: Joueur -> Maybe Coup
getCoupM (Ia coup) = coup
getCoupM (Humain c0 c1 _) =
    if isJust c0 && isJust c1 then Just (fromJust c0, fromJust c1) else Nothing

getCaseInitialeM :: Joueur -> Maybe Case
getCaseInitialeM (Ia coup) = if isNothing coup then Nothing else Just ((fst.fromJust) coup)
getCaseInitialeM (Humain c0 _ _) = if isNothing c0 then Nothing else c0

-- private
getCoupIA :: Plateau -> Maybe Coup
getCoupIA p =
    let coups = getCoupsPossibles p
    in if null coups
       then Nothing
       else let adverses = (snd.getCourantsAdverses) p
                captures = [ c | c@(_,c1) <- coups, c1 `elem` adverses]
            in Just (head (if null captures then coups else captures))
